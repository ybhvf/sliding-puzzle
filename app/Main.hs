module Main where

import Puzzle
import System.Random

main :: IO ()
main = do
    let puz = puzzle 7 5
    print puz
    putStrLn "7 rows x 5 columns puzzle"

    let puz' = move puz DOWN
    print puz'
    putStrLn "Move the empty space down"

    let puzA = rotate puz' 3 4 CW
    print puzA
    putStrLn "Rotate 3,4 loop CW"

    let puzB = rotate puzA 3 (-2) CCW
    print puzB
    putStrLn "Rotate 3,-2 loop CCW"

    gen <- newStdGen
    let puzC = shuffle puzB gen
    print puzC
    putStrLn "Shuffled"

    putStrLn "\nAnimation of history"
    animate puzC
