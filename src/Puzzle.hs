module Puzzle where

import Control.Concurrent
import Data.List
import Data.Matrix
import Data.Maybe
import System.Random
import System.Random.Shuffle (shuffle')

data Direction = LEFT | RIGHT | UP | DOWN deriving (Eq)
data Rotation = CW | CCW deriving (Eq)

type Index = (Int, Int)

offset :: Direction -> Index -> Index
offset LEFT (x, y) = (x, y - 1)
offset RIGHT (x, y) = (x, y + 1)
offset UP (x, y) = (x - 1, y)
offset DOWN (x, y) = (x + 1, y)

fac :: Int -> Int
fac 0 = 1
fac n = n * fac (n - 1)

-- Example puzzle matrix, where "0" is the empty square:
--
-- | 1 4 3 |
-- | 2 6 0 |
-- | 7 5 8 |
data Puzzle = Puzzle {
    empty :: Index,    -- The index of the empty tile
    mat :: Matrix Int, -- A matrix of tiles, stored as unique integers
    history :: [Matrix Int]
}


instance Show Puzzle where
    show (Puzzle _ mat _) = "\n" ++ show mat


animate :: Puzzle -> IO ()
animate puz = do
    let lines = nrows (mat puz) + 2

    -- escape ++ "A" moves cursor up N lines
    -- escape ++ "B" moves cursor down N lines
    let escape = "\ESC[" ++ (show lines)

    mapM_ (\puz -> do
        putStr (show puz ++ escape ++ "A\n")
        threadDelay 500000) (history puz ++ [mat puz])

    putStr (escape ++ "B\n")


-- Construct a puzzle of a size (rows,cols)
puzzle rows cols = Puzzle (1, 1)
                          (fromList rows cols [0..rows*cols])
                          []


findTile :: Matrix Int -> Int -> (Int, Int)
findTile mat tile = do
    let index = fromJust $ elemIndex tile (toList mat)
    ((quot index (ncols mat)) + 1, (mod index (ncols mat)) + 1)


-- Shuffle a puzzle
shuffle (Puzzle _ mat hist) gen = do
    let new_list = shuffle' (toList mat) ((nrows mat) * (ncols mat)) gen
    let new_mat = fromList (nrows mat) (ncols mat) new_list
    let new_empty = findTile new_mat 0
    Puzzle new_empty new_mat (hist ++ [mat])


-- Move the empty tile in a given direction
move :: Puzzle -> Direction -> Puzzle
move (Puzzle empty mat hist) dir = do
    let new_empty = offset dir empty
    let swap_num = mat ! new_empty
    Puzzle new_empty
           (setElem 0 new_empty (setElem swap_num empty mat))
           (hist ++ [mat])


-- Repeated move the empty tile
multiMove :: Puzzle -> [(Direction, Int)] -> Puzzle
multiMove puz moves = do
    foldl (\puz (dir, size) -> foldl move puz (take size (repeat dir))) puz moves


-- Rotate a loop
--
-- For example, (rotate -2 -2 CCW) would produce this transformation:
--
-- | A B C D E |     | A C D H E |                     C D H
-- | E F G H I |     | E B G 0 I |                     B   0
-- | J K L 0 N | --> | J F K L N |, where the loop is: F K L
-- | O P Q R S |     | O P Q R S |
--
rotate :: Puzzle -> Int -> Int -> Rotation -> Puzzle
rotate puz rows cols dir = do
    -- row first
    let row_first = [abs rows - 1, abs cols - 1, abs rows - 1, abs cols - 2]
    -- col first
    let col_first = [abs cols - 1, abs rows - 1, abs cols - 1, abs rows - 2]

    let moves | dir == CW  && rows >= 0 && cols <  0 = [LEFT, DOWN, RIGHT, UP] -- bottom left
              | dir == CW  && rows >= 0 && cols >= 0 = [DOWN, RIGHT, UP, LEFT] -- bottom right
              | dir == CW  && rows <  0 && cols >= 0 = [RIGHT, UP, LEFT, DOWN] -- top right
              | dir == CW  && rows <  0 && cols <  0 = [UP, LEFT, DOWN, RIGHT] -- top left
              | dir == CCW && rows >= 0 && cols <  0 = [DOWN, LEFT, UP, RIGHT] -- bottom left
              | dir == CCW && rows >= 0 && cols >= 0 = [RIGHT, DOWN, LEFT, UP] -- bottom right
              | dir == CCW && rows <  0 && cols >= 0 = [UP, RIGHT, DOWN, LEFT] -- top right
              | dir == CCW && rows <  0 && cols <  0 = [LEFT, UP, RIGHT, DOWN] -- top left

    let puz2 | moves!!0 == UP   || moves!!0 == DOWN  = multiMove puz (zip moves row_first)
             | moves!!0 == LEFT || moves!!0 == RIGHT = multiMove puz (zip moves col_first)
    puz2


walkEmpty :: Puzzle -> (Int, Int) -> Puzzle
walkEmpty puz (dst_row, dst_col) = do
    let row_dir | (fst (empty puz)) < dst_row = DOWN
                | otherwise                   = UP

    let col_dir | (snd (empty puz)) < dst_col = RIGHT
                | otherwise                   = LEFT

    multiMove puz [(row_dir, dst_row - (fst (empty puz))),
                   (col_dir, dst_col - (snd (empty puz)))]


walkTile :: Puzzle -> Int -> (Int, Int) -> Puzzle
walkTile puz tile (dst_row, dst_col) = do
    -- find tile
    let (src_row, src_col) = findTile (mat puz) tile

    let empty_dst = (maximum(src_row, dst_row), maximum(src_col, dst_col))

    let puz' = walkEmpty puz empty_dst

    -- TODO

    puz'
